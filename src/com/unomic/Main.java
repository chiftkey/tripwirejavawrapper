package com.unomic;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(String[] args) {

        //기본 사이클 타임은 10분;
        int cycleTime;

        if(args.length == 0) {
            System.out.println("파라미터 값이 없습니다. 기본값 10분 주기로 실행됩니다.");
            cycleTime = 10;
        }else{
            try {
                //첫번째 파라미터 값으로 시간 설정
                cycleTime = Integer.valueOf(args[0]);
                System.out.println(cycleTime + "분 주기로 실행됩니다.");
            } catch (NumberFormatException e) {
                //입력이 이상할 경우 디폴트 값으로 설정'
                cycleTime = 10;
                System.out.println("입력포맷이 이상합니다. 기본값 10분 주기로 실행됩니다.");
            }
        }

        while(true) {
            try {
                ProcessBuilder pBuilder = new ProcessBuilder();
                pBuilder.command("tripwire", "--check");
                pBuilder.redirectOutput(Paths.get("Log_FileCheck.txt").toFile());
                Process pc = pBuilder.start();
                pc.waitFor();
                System.out.println("파일기록 완료");
                Thread.sleep(cycleTime * 60000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (IOException e){
                e.printStackTrace();
                break;
            }
        }
        System.out.println("Tripwire Java Wrapper 종료..");
    }
}
