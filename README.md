# TripWire Java Wrapper
---
## 목차

[TOC]

---

## 실행환경

* Ubuntu 16.04.05 LTS ARM (ARTIK ver)
* Java 1.8 이상
    * 없으면 `sudo apt install default-jre`
* Tripwire가 설치되어 있어야 함
    * 없으면 `sudo apt install tripwire`

## Tripwire 설치 (동영상 가이드)
* https://vimeo.com/299816505
* 비밀번호 : `unomic`

## Tripwire 설치 (패키지 설치시 키를 생성하지 않았을 경우)

1. `sudo apt install tripwire`
2. 설치 완료 후 `/etc/tripwire/`로 이동
3. Local-Key 생성
    1. /etc/tripwire$ `sudo twadmin --generate-keys -L ${HOSTNAME}-local.key`
4. Site-Key 생성
    1. /etc/tripwire$ `sudo twadmin --generate-keys -S site.key`
5. 설정파일 암호화
    1. /etc/tripwire$ `sudo twadmin --create-cfgfile -S site-key twcfg.txt`
    2. 옵션줄 때 `--create-cfgfile` 대신 `-m F` 사용 가능
        1. `sudo twadmin -m F -S site-key twcfg.txt`
    3. 위에서 지정한 site-key passpharase 입력
6. 정책파일 암호화
    1. /etc/tripwire$ `twadmin --create-polfile -S site.key twpol.txt`
    2. 옵션줄 때 `--create-polfile` 대신 `-m P` 사용가능
        1. `sudo twadmin -m P -S site.key twpol.txt`
    3. 위에서 지정한 site-key passpharase 입력
7. 설정파일과 정책파일의 암호화가 끝나면 원본은 삭제하는 것이 좋다


   
## Tripwire 사용  준비
1. 정책파일 수정
    1. ARTIK710의 OS인 `Ubutnu 16.04.5 LTS ARM`에 맞게 정책 수정
    2. `cd /etc/tripwire`
    3. `[#/etc/tripwire]` `sudo vi twpol.txt`
        1. 문서 제일 아래의 **`정책파일 수정`** 파트 참고..
        2. 수정된 정책파일 적용
            1. `[#/etc/tripwire]` `sudo twadmin -m P -S site.key twpol.txt`
   

2. tripwire 초기화
    1. $ `sudo tripwire --init`
    2. 위에서 지정한 **Local-Key** 비밀번호 입력
    3. **`이 과정이 상당시간 소요될 수 잇음..`**
    4. 이 과정을 거치면, `twcfg.txt`에 지정된 `DBFILE`경로에 데이터베이스 파일이 생긴다.

## Java 프로세스 실행

1. jar빌드
2. 파라미터값으로 반복시간을 조절할 수 있음 (단위는 분)
3. `java -jar TripWireJavaWrapper 5`
4. 파라미터를 입력하지 않으면 기본으로 10분 단위로 루핑 반복
5. `Log_FileCheck.txt` 에 리포트 기록


## Tripwire를 재설치 하고 싶을 때
1. `sudo apt purge tripwire`
2. `sudo apt autoremove`

---

## 정책파일 수정
* `/etc/tripwire/twpol.txt`를 수정한다.
        
```
    ...
    ...
    {
        /etc/init.d		-> $(SEC_BIN) ;
        #/etc/rc.boot		-> $(SEC_BIN) ;
        ...
        ...
    }
    ...
    ...
    ...
    {
        /root				-> $(SEC_CRIT) ; # Catch all additions to /root
    #펌웨어 업데이트에 사용되는 경로
        !/root/key/;
        !/root/nc_agent/;
        !/root/link_update-1.0/;
        !/root/checker;
    #존재하지 않는 파일은 주석처리
        #/root/mail			-> $(SEC_CONFIG) ;
        #/root/Mail			-> $(SEC_CONFIG) ;
        #/root/.xsession-errors		-> $(SEC_CONFIG) ;
        #/root/.xauth			-> $(SEC_CONFIG) ;
        #/root/.tcshrc			-> $(SEC_CONFIG) ;
        #/root/.sawfish			-> $(SEC_CONFIG) ;
        #/root/.pinerc			-> $(SEC_CONFIG) ;
        #/root/.mc			-> $(SEC_CONFIG) ;
        #/root/.gnome_private		-> $(SEC_CONFIG) ;
        #/root/.gnome-desktop		-> $(SEC_CONFIG) ;
        #/root/.gnome			-> $(SEC_CONFIG) ;
        #/root/.esd_auth			-> $(SEC_CONFIG) ;
        #/root/.elm			-> $(SEC_CONFIG) ;
        #/root/.cshrc		        -> $(SEC_CONFIG) ;
        #/root/.bashrc			-> $(SEC_CONFIG) ;
        #/root/.bash_profile		-> $(SEC_CONFIG) ;
        #/root/.bash_logout		-> $(SEC_CONFIG) ;
        #/root/.bash_history		-> $(SEC_CONFIG) ;
        #/root/.amandahosts		-> $(SEC_CONFIG) ;
        #/root/.addressbook.lu		-> $(SEC_CONFIG) ;
        #/root/.addressbook		-> $(SEC_CONFIG) ;
        #/root/.Xresources		-> $(SEC_CONFIG) ;
        #/root/.Xauthority		-> $(SEC_CONFIG) -i ; # Changes Inode number on login
        #/root/.ICEauthority		    -> $(SEC_CONFIG) ;
    }
    ...
    ...
    ...
    {
            /dev		-> $(Device) ;
            #오류를 발생시키는 아래항목들 예외처리 추가
            !/dev/hugepages;
            !/dev/mqueue;
            !/dev/pts;
            !/dev/shm;
            #/proc		-> $(Device) ;
    }
```